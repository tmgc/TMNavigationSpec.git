#
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― # 
  s.name         = "TMNavigation"
  s.version      = "0.0.955"
  s.summary      = "TM 导航组件"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/TMNavigationSpec.git", :tag => s.version }
  s.source_files = 'TMFramework/TMNavigation.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/TMNavigation.framework'
  s.resources = "TMFramework/TMNavigation.bundle"
  s.requires_arc = true
  s.dependency "TMSDK"
  #s.dependency "TMUserCenter", '>= 0.1.89'
  s.dependency "TMUserCenter"
  s.dependency "AFNetworking"
  s.dependency "Masonry"
  s.dependency "SDWebImage"
  s.dependency "SVProgressHUD"
  s.dependency "TMShare"
  s.dependency "Hero"
  s.dependency "ZFPlayer"
  s.dependency "ZFPlayer/ControlView"
  s.dependency "ZFPlayer/AVPlayer"
  s.dependency "KTVHTTPCache"

s.xcconfig = {
  'VALID_ARCHS' =>  'armv7 x86_64 arm64',
}
end
