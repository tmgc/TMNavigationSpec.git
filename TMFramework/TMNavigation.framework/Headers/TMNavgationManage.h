//
//  TMNavgationManage.h
//  TMNavigation
//
//  Created by rxk on 2021/1/25.
//  Copyright © 2021 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TMNavigationImageSuffixModel;
@interface TMNavgationManage : NSObject
@property (nonatomic, assign) double enterBackgroundTime;
///导航栏样式数据
@property (nonatomic, strong) NSDictionary *config;
///图片拼接策略
@property (nonatomic, strong) TMNavigationImageSuffixModel *imageSuffix;
///新版是否开启转码
@property (nonatomic, assign) BOOL isTranscode;
///新版接口转码接口是否能请求通
@property (nonatomic, assign) BOOL isTranscodeSuccess;
+ (TMNavgationManage *)instance;

+ (UIColor *)tmnav_colorValueFromString:(NSString *)colorStr;
@end


@interface TMNavigationImageSuffixModel : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *check_big;
@property (nonatomic, copy) NSString *detail_pic;
@property (nonatomic, copy) NSString *list_big;
@property (nonatomic, copy) NSString *list_mid;
@property (nonatomic, copy) NSString *list_small;
@end
NS_ASSUME_NONNULL_END
