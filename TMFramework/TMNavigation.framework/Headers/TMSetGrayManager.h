//
//  TMSetGrayManager.h
//  Cordova
//
//  Created by cy on 2023/3/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TMSetGrayManager : NSObject

///置灰
+ (void)showGray;

///置灰消失
+ (void)dismissGray;

@end

NS_ASSUME_NONNULL_END
