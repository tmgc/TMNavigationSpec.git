//
//  TMNavigationController.h
//  TMNavigation
//
//  Created by rxk on 2019/1/2.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TMNavigationController : UINavigationController
@property (nonatomic, strong) NSDictionary *config;

@end

NS_ASSUME_NONNULL_END
