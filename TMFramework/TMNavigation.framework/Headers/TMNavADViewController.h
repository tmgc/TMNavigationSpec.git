//
//  TMNavADViewController.h
//  TMNavigation
//
//  Created by rxk on 2021/7/13.
//  Copyright © 2021 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMNavAdImageShowView.h"
NS_ASSUME_NONNULL_BEGIN

//广告图消失通知
#define kTMNAVADDISMISS @"kTMNAVADDISMISS"

@interface TMNavADViewController : UIView
@property (nonatomic, strong) TMNavAdImageShowView * _Nullable lauchImageView;
@property (nonatomic, copy) void(^tmnav_dismissController)(BOOL isClickADView,NSString * url);

- (void)tmnav_showADViewWithADModel:(TMNavAdModel *)adModel;
- (void)tmnav_startCountdown;


@end

NS_ASSUME_NONNULL_END
